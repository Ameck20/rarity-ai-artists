const ErrorPage = ({ statusCode }) => {
    return (
        <div
            className="container flex flex-col items-center justify-center h-screen mx-auto max-w-2xl"
            style={{ minHeight: 'calc(100vh - 64px)' }}
        >
            <h1 className="text-5xl font-bold">{statusCode}</h1>
            <p className="text-2xl">
                {statusCode
                    ? `An error ${statusCode} occurred on server`
                    : 'An error occurred on client'}
            </p>
        </div>
    );
}

Error.getInitialProps = ({ res, err }) => {
    const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
    return { statusCode };
};

export default ErrorPage;
