import React, { useState } from 'react';
import Main from '../templates/Main';

const ComponentName = () => {
    return (
        <Main>
            <section className="relative py-12">
                <div className="relative px-4 mx-auto max-w-7xl sm:px-6 lg:px-8">
                    <div className="max-w-md mx-auto text-center lg:max-w-lg">
                        <h1 className="text-3xl font-bold text-gray-900 sm:text-4xl lg:text-5xl">
                            Discover, Collect & Sell Incredible NFTs
                        </h1>
                        <p className="mt-4 text-lg font-normal text-gray-500 sm:text-xl">
                            More than 2,200 artworks available for auction. Get your artwork now.
                        </p>
                    </div>

                    <div className="mt-12">
                        <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-6 py-4 mx-auto -my-4 justify-center">
                            <div
                                className="overflow-hidden w-72 transition-all duration-200 transform bg-white mx-auto border border-gray-200 rounded-md snap-start scroll-ml-6 shrink-0 hover:shadow-lg hover:-translate-y-1 group">
                                <a href="#" title="" className="block overflow-hidden aspect-w-1 aspect-h-1">
                                    <img className="object-cover w-full h-full transition-all duration-200 group-hover:scale-110"
                                        src="https://landingfoliocom.imgix.net/store/collection/niftyui/images/hero-single/2/image-1.png" alt="" />
                                </a>
                                <div className="p-4">
                                    <div className="flex items-center justify-between space-x-6">
                                        <p className="flex-1 text-base font-medium text-gray-900">
                                            <a href="#" title="">
                                                Faratey58
                                            </a>
                                        </p>
                                        <a href="#" title="" className="relative inline-flex items-center justify-center shrink-0 w-7 h-7">
                                            <div className="absolute inset-0">
                                                <img className="w-full h-full object-coveer"
                                                    src="https://landingfoliocom.imgix.net/store/collection/niftyui/images/featured-drops-marketplace/9/avatar-background.png" alt="" />
                                            </div>
                                            <div className="relative text-xs font-bold text-white uppercase">
                                                FA
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div className="p-4 border-t border-gray-200">
                                    <div className="flex items-center justify-between">
                                        <p className="text-sm font-medium text-gray-500">
                                            Price
                                        </p>
                                        <p className="text-sm font-medium text-gray-900">
                                            1.4 MATIC <span className="text-gray-500">(2,352 USD)</span>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div
                                className="overflow-hidden w-72 transition-all duration-200 transform bg-white mx-auto border border-gray-200 rounded-md snap-start scroll-ml-6 shrink-0 hover:shadow-lg hover:-translate-y-1 group">
                                <a href="#" title="" className="block overflow-hidden aspect-w-1 aspect-h-1">
                                    <img className="object-cover w-full h-full transition-all duration-200 group-hover:scale-110"
                                        src="https://landingfoliocom.imgix.net/store/collection/niftyui/images/hero-single/2/image-2.png" alt="" />
                                </a>
                                <div className="p-4">
                                    <div className="flex items-center justify-between space-x-6">
                                        <p className="flex-1 text-base font-medium text-gray-900">
                                            <a href="#" title="">
                                                Wildwildworld
                                            </a>
                                        </p>
                                        <a href="#" title="" className="relative inline-flex items-center justify-center shrink-0 w-7 h-7">
                                            <div className="absolute inset-0">
                                                <img className="w-full h-full object-coveer"
                                                    src="https://landingfoliocom.imgix.net/store/collection/niftyui/images/featured-drops-marketplace/9/avatar-background.png" alt="" />
                                            </div>
                                            <div className="relative text-xs font-bold text-white uppercase">
                                                Wd
                                            </div>
                                        </a>
                                    </div>
                                </div>

                                <div className="p-4 border-t border-gray-200">
                                    <div className="flex items-center justify-between">
                                        <p className="text-sm font-medium text-gray-500">
                                            Price
                                        </p>
                                        <p className="text-sm font-medium text-gray-900">
                                            1.4 MATIC <span className="text-gray-500">(2,352 USD)</span>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div
                                className="overflow-hidden w-72 transition-all duration-200 transform bg-white mx-auto border border-gray-200 rounded-md snap-start scroll-ml-6 shrink-0 hover:shadow-lg hover:-translate-y-1 group">
                                <a href="#" title="" className="block overflow-hidden aspect-w-1 aspect-h-1">
                                    <img className="object-cover w-full h-full transition-all duration-200 group-hover:scale-110"
                                        src="https://landingfoliocom.imgix.net/store/collection/niftyui/images/hero-single/2/image-3.png" alt="" />
                                </a>
                                <div className="p-4">
                                    <div className="flex items-center justify-between space-x-6">
                                        <p className="flex-1 text-base font-medium text-gray-900">
                                            <a href="#" title="">
                                                ZaTaS
                                            </a>
                                        </p>
                                        <a href="#" title="" className="relative inline-flex items-center justify-center shrink-0 w-7 h-7">
                                            <div className="absolute inset-0">
                                                <img className="w-full h-full object-coveer"
                                                    src="https://landingfoliocom.imgix.net/store/collection/niftyui/images/featured-drops-marketplace/9/avatar-background.png" alt="" />
                                            </div>
                                            <div className="relative text-xs font-bold text-white uppercase">
                                                ZS
                                            </div>
                                        </a>
                                    </div>
                                </div>

                                <div className="p-4 border-t border-gray-200">
                                    <div className="flex items-center justify-between">
                                        <p className="text-sm font-medium text-gray-500">
                                            Price
                                        </p>
                                        <p className="text-sm font-medium text-gray-900">
                                            1.4 MATIC <span className="text-gray-500">(2,352 USD)</span>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div
                                className="overflow-hidden w-72 transition-all duration-200 transform bg-white mx-auto border border-gray-200 rounded-md snap-start scroll-ml-6 shrink-0 hover:shadow-lg hover:-translate-y-1 group">
                                <a href="#" title="" className="block overflow-hidden aspect-w-1 aspect-h-1">
                                    <img className="object-cover w-full h-full transition-all duration-200 group-hover:scale-110"
                                        src="https://landingfoliocom.imgix.net/store/collection/niftyui/images/hero-single/2/image-1.png" alt="" />
                                </a>
                                <div className="p-4">
                                    <div className="flex items-center justify-between space-x-6">
                                        <p className="flex-1 text-base font-medium text-gray-900">
                                            <a href="#" title="">
                                                Faratey58
                                            </a>
                                        </p>
                                        <a href="#" title="" className="relative inline-flex items-center justify-center shrink-0 w-7 h-7">
                                            <div className="absolute inset-0">
                                                <img className="w-full h-full object-coveer"
                                                    src="https://landingfoliocom.imgix.net/store/collection/niftyui/images/featured-drops-marketplace/9/avatar-background.png" alt="" />
                                            </div>
                                            <div className="relative text-xs font-bold text-white uppercase">
                                                FA
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div className="p-4 border-t border-gray-200">
                                    <div className="flex items-center justify-between">
                                        <p className="text-sm font-medium text-gray-500">
                                            Price
                                        </p>
                                        <p className="text-sm font-medium text-gray-900">
                                            1.4 MATIC <span className="text-gray-500">(2,352 USD)</span>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div
                                className="overflow-hidden w-72 transition-all duration-200 transform bg-white mx-auto border border-gray-200 rounded-md snap-start scroll-ml-6 shrink-0 hover:shadow-lg hover:-translate-y-1 group">
                                <a href="#" title="" className="block overflow-hidden aspect-w-1 aspect-h-1">
                                    <img className="object-cover w-full h-full transition-all duration-200 group-hover:scale-110"
                                        src="https://landingfoliocom.imgix.net/store/collection/niftyui/images/hero-single/2/image-2.png" alt="" />
                                </a>
                                <div className="p-4">
                                    <div className="flex items-center justify-between space-x-6">
                                        <p className="flex-1 text-base font-medium text-gray-900">
                                            <a href="#" title="">
                                                Wildwildworld
                                            </a>
                                        </p>
                                        <a href="#" title="" className="relative inline-flex items-center justify-center shrink-0 w-7 h-7">
                                            <div className="absolute inset-0">
                                                <img className="w-full h-full object-coveer"
                                                    src="https://landingfoliocom.imgix.net/store/collection/niftyui/images/featured-drops-marketplace/9/avatar-background.png" alt="" />
                                            </div>
                                            <div className="relative text-xs font-bold text-white uppercase">
                                                Wd
                                            </div>
                                        </a>
                                    </div>
                                </div>

                                <div className="p-4 border-t border-gray-200">
                                    <div className="flex items-center justify-between">
                                        <p className="text-sm font-medium text-gray-500">
                                            Price
                                        </p>
                                        <p className="text-sm font-medium text-gray-900">
                                            1.4 MATIC <span className="text-gray-500">(2,352 USD)</span>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div
                                className="overflow-hidden w-72 transition-all duration-200 transform bg-white mx-auto border border-gray-200 rounded-md snap-start scroll-ml-6 shrink-0 hover:shadow-lg hover:-translate-y-1 group">
                                <a href="#" title="" className="block overflow-hidden aspect-w-1 aspect-h-1">
                                    <img className="object-cover w-full h-full transition-all duration-200 group-hover:scale-110"
                                        src="https://landingfoliocom.imgix.net/store/collection/niftyui/images/hero-single/2/image-3.png" alt="" />
                                </a>
                                <div className="p-4">
                                    <div className="flex items-center justify-between space-x-6">
                                        <p className="flex-1 text-base font-medium text-gray-900">
                                            <a href="#" title="">
                                                ZaTaS
                                            </a>
                                        </p>
                                        <a href="#" title="" className="relative inline-flex items-center justify-center shrink-0 w-7 h-7">
                                            <div className="absolute inset-0">
                                                <img className="w-full h-full object-coveer"
                                                    src="https://landingfoliocom.imgix.net/store/collection/niftyui/images/featured-drops-marketplace/9/avatar-background.png" alt="" />
                                            </div>
                                            <div className="relative text-xs font-bold text-white uppercase">
                                                ZS
                                            </div>
                                        </a>
                                    </div>
                                </div>

                                <div className="p-4 border-t border-gray-200">
                                    <div className="flex items-center justify-between">
                                        <p className="text-sm font-medium text-gray-500">
                                            Price
                                        </p>
                                        <p className="text-sm font-medium text-gray-900">
                                            1.4 MATIC <span className="text-gray-500">(2,352 USD)</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </Main>
    )
}
export default ComponentName;