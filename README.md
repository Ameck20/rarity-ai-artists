# Sample Hardhat Project

This project demonstrates a basic Hardhat use case. It comes with a sample contract, a test for that contract, and a script that deploys that contract.

Try running some of the following tasks:

```shell
npx hardhat help
npx hardhat test
REPORT_GAS=true npx hardhat test
npx hardhat node
```

## Compile the contracts

```python
npx hardhat compile
```

## Deploying the contract on testnet

```shell
npx hardhat run scripts/deploy.js --network mumbai
npx hardhat run scripts/deploy-eth.js --network sepolia
```

## Deploying the contract on mainnet

```shell
npx hardhat run scripts/deploy.js --network polygon
npx hardhat run scripts/deploy-eth.js --network ethereum
```

## Upgrading a contract

```shell
npx hardhat run scripts/upgrade-contract.js --network {network}
npx hardhat run scripts/upgrade-contract-eth.js --network {network}
```