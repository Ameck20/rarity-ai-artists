export const AppConfig = {
  site_name: 'Rarity AI Artists',
  title: 'Rarity AI Artists',
  description: 'Where the talents of ai proudly create masterpieces.',
  locale: 'en',
};
