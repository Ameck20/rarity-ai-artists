import { createSlice } from '@reduxjs/toolkit';
import { UNAUTHORIZED } from '../../utils/constants';

const userSlice = createSlice({
  name: 'user',
  initialState: {
    currentUser: null,
    loading: false,
    error: false,
    status: null,
    request: null,
    state: null,
  },
  reducers: {
    USER_START: (state) => {
      state.loading = true;
    },
    USER_SUCCESS: (state) => {
      state.loading = false;
    },
    REQUEST: (state, action) => {
      if (action.payload && typeof action.payload === 'object') {
        state.request = action.payload.request;
        state.state = action.payload.data;
      } else {
        state.request = action.payload;
      }
    },
    GET_USER: (state, action) => {
      state.currentUser = {
        ...state.currentUser,
        ...action.payload,
      };
      state.loading = false;
      state.status = action.payload?.status;
    },
    USER_ERROR: (state, action) => {
      state.loading = false;
      state.error = action.payload;
    },
    SIGN_IN: (state, action) => {
      state.currentUser = {
        ...action.payload,
      };
      state.loading = false;
    },
    SIGN_OUT: (state, action) => {
      state.currentUser = null;
      state.loading = false;
      state.error = false;
      state.status = null;
      state.request = null;
      state.state = null;
    },
    REQUEST_COLLECTION: (state, action) => {
      state.currentUser = {
        ...state.currentUser,
        collections: action.payload,
      };
      state.loading = false;
    },
    REQUEST_NFTS: (state, action) => {
      state.currentUser = {
        ...state.currentUser,
        nfts: action.payload,
      };
      state.loading = false;
    },
    RESET: (state) => {
      state.loading = false;
    },
    EXPIRED: (state) => {
      state.isLoading = false;
      state.error = UNAUTHORIZED;
    },
    LOGIN_RESET: (state) => {
      state.currentUser = null;
    },
  },
});

export const {
  USER_START,
  USER_SUCCESS,
  GET_USER,
  USER_ERROR,
  SIGN_IN,
  SIGN_OUT,
  REQUEST,
  REQUEST_COLLECTION,
  REQUEST_NFTS,
  REQUEST_FAVORITES,
  RESET,
  EXPIRED,
  LOGIN_RESET,
} = userSlice.actions;
export default userSlice.reducer;
