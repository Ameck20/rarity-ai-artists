/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';

const nftSlice = createSlice({
  name: 'nfts',
  initialState: {
    collections: [],
    topCollections: [],
    nfts: [],
    blockchains: [],
    left: 0,
    loading: false,
    isLoading: false,
    error: false,
  },
  reducers: {
    NFTS_START: (state) => {
      state.error = null;
      state.loading = true;
    },
    NFTS_SUCCESS: (state) => {
      state.loading = false;
      state.isLoading = false;
    },
    GET_NFTS_COLLECTIONS: (state, action) => {
      state.collections = action.payload;
      state.loading = false;
    },
    GET_NFTS_TOP_COLLECTIONS: (state, action) => {
      state.topCollections = action.payload;
      state.loading = false;
    },
    GET_NFTS: (state, action) => {
      state.nfts = action.payload;
    },
    GET_NFTS_LEFT: (state, action) => {
      state.left = action.payload;
    },
    GET_BLOCKCHAINS: (state, action) => {
      state.blockchains = action.payload;
      state.loading = false;
    },
    NFTS_ERROR: (state, action) => {
      state.loading = false;
      state.error = action.payload;
    },
    RESET_NFTS: (state) => {
      state.nfts = [];
    },
  },
});

export const {
  NFTS_START,
  NFTS_SUCCESS,
  GET_NFTS_COLLECTIONS,
  GET_NFTS_TOP_COLLECTIONS,
  GET_NFTS,
  GET_BLOCKCHAINS,
  NFTS_ERROR,
  RESET_NFTS,
  GET_NFTS_LEFT,
} = nftSlice.actions;
export default nftSlice.reducer;
