// SPDX-License-Identifier: MIT
pragma solidity ^0.8.9;

import "@openzeppelin/contracts-upgradeable/token/ERC1155/ERC1155Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/CountersUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/StringsUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/StringsUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

contract Starmint is
    Initializable,
    ERC1155Upgradeable,
    OwnableUpgradeable
{
    using CountersUpgradeable for CountersUpgradeable.Counter;
    CountersUpgradeable.Counter private _tokenIds;

    uint256 private _royaltyBasisPoints; // Pourcentage des royalties en basis points (1/10000)
    address payable private _royaltyAddress;

    struct RoyaltyReceiver {
        address payable receiver;
        uint256 basisPoints;
        address payable royaltyAddress;
    }

    mapping(uint256 => RoyaltyReceiver[]) private _royaltyReceivers;
    mapping(uint256 => string) private _tokenURIs;

    string public name;
    string public metadataURI;

    function initialize(
        string memory _name,
        uint256 royaltyBasisPoints,
        address payable royaltyAddress,
        string memory _contractURI
    ) external initializer {
        name = _name;
        metadataURI = _contractURI;
        _royaltyBasisPoints = royaltyBasisPoints;
        _royaltyAddress = royaltyAddress;
        __ERC1155_init("");
        __Ownable_init();
    }

    function contractURI() public view returns (string memory) {
        return metadataURI;
    }

    function changeRoyaltyAddress(address payable newAddress) public onlyOwner {
        _royaltyAddress = newAddress;
    }

    function transferOwnership(address newOwner) public override onlyOwner {
        require(newOwner != address(0), "New owner address cannot be zero");
        super.transferOwnership(newOwner);
    }

    function setTokenURI(uint256 tokenId, string memory newUri) public {
        require(bytes(_tokenURIs[tokenId]).length == 0, "Cannot set uri");
        _tokenURIs[tokenId] = newUri;
    }

    function setRoyaltyBasisPoints(
        uint256 newRoyaltyBasisPoints
    ) public onlyOwner {
        _royaltyBasisPoints = newRoyaltyBasisPoints;
    }

    function mint(
        address account,
        uint256 amount,
        string memory newUri,
        bytes memory data
    ) public payable {
        _tokenIds.increment();
        uint256 newTokenId = _tokenIds.current();
        _mint(account, newTokenId, amount, data);
        setTokenURI(newTokenId, newUri);
        RoyaltyReceiver memory receiver;
        receiver.receiver = payable(msg.sender);
        receiver.basisPoints = _royaltyBasisPoints;
        _royaltyReceivers[newTokenId].push(receiver);
    }

    function mintBatch(
        address to,
        uint256[] memory ids,
        uint256[] memory amounts,
        bytes memory data
    ) public payable {
        _mintBatch(to, ids, amounts, data);
    }

    function sell(
        uint256 tokenId,
        uint256 amount,
        address payable buyer
    ) public payable {
        require(
            balanceOf(msg.sender, tokenId) > 0,
            "Not the owner of the token"
        ); // Vérification du propriétaire du NFT
        require(amount > 0, "Amount must be greater than 0");

        // Calcul du montant des royalties
        uint256 royaltyAmount = (_royaltyBasisPoints * amount) / 10000;
        require(msg.value >= royaltyAmount, "Insufficient royalty payment");

        // Transfert des royalties à l'adresse spécifiée
        uint256 sellerAmount = amount - royaltyAmount;
        payable(_royaltyAddress).transfer(royaltyAmount);

        // Transfert du reste du montant au vendeur
        safeTransferFrom(msg.sender, buyer, tokenId, sellerAmount, "");

        // Remboursement du surplus si nécessaire
        if (msg.value > royaltyAmount) {
            payable(msg.sender).transfer(msg.value - royaltyAmount);
        }
    }

    function withdraw(uint256 amount) public onlyOwner {
        uint256 contractBalance = address(this).balance;
        require(contractBalance > 0, "No balance to withdraw");
        require(amount <= contractBalance, "Insufficient balance to withdraw");
        payable(owner()).transfer(amount);
    }

    function uri(uint256 tokenId) public view override returns (string memory) {
        return _tokenURIs[tokenId];
    }
}
